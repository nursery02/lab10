package vn.tinylab.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
public class CompositeKey implements Serializable {
    @Column(name = "product_id", nullable = false)
    private long productId;

    @Column(name = "customer_id", nullable = false)
    private long customerId;

    /** getters and setters **/
}