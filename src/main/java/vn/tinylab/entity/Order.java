package vn.tinylab.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity(name = "Orders")
@Getter
@Setter
public class Order implements Serializable {
    @EmbeddedId
    private CompositeKey compositeKey;

    private long quantity;
    private double totalPrice;
}
