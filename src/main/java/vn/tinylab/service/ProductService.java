package vn.tinylab.service;

import org.springframework.transaction.annotation.Transactional;
import vn.tinylab.entity.Product;
import vn.tinylab.model.request.ProductRequest;

public interface ProductService {
    @Transactional
    Product createProduct(ProductRequest productRequest);

    @Transactional
    Product updateProduct(long existedProductId, long quantity);
}
