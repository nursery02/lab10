package vn.tinylab.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.tinylab.entity.AbstractUser;
import vn.tinylab.entity.Customer;
import vn.tinylab.model.request.UserRequest;
import vn.tinylab.repo.CustomerRepository;
import vn.tinylab.service.RegisterService;

@Service
@AllArgsConstructor
public class RegisterCustomerServiceImpl implements RegisterService {
    private final CustomerRepository customerRepository;

    @Override
    public AbstractUser createUser(UserRequest customer) {
        customer.setName(customer.getName() == null ? "" : customer.getName().trim());
        customer.setCredit(customer.getCredit() == null ? 0.0 : customer.getCredit());

        Customer createdCustomer = new Customer();
        createdCustomer.setName(customer.getName());
        createdCustomer.setCredit(customer.getCredit());

        return customerRepository.save(createdCustomer);
    }

}
