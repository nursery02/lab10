package vn.tinylab.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.tinylab.entity.AppUser;
import vn.tinylab.model.request.ResponseModel;
import vn.tinylab.service.impl.AppUserServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    AppUserServiceImpl userServiceImpl;

    @PostMapping("")
    @ApiOperation(value = "Create new user", notes = "Create new user for system")
    public ResponseEntity<AppUser> createUser(@RequestBody AppUser user) throws JsonProcessingException {
        AppUser dt = userServiceImpl.createNewUser(user);
        return ResponseEntity.ok(dt);
    }

    @GetMapping("")
    @Cacheable(value = "AppUser", key = "{#email,#page,#size, #sortby}")
    public ResponseModel getAllTutorials(
            @RequestParam(required = false) String email,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "3") int size,
            @RequestParam(defaultValue = "email") String sortby
    ) {
        try {
            List<AppUser> tutorials = new ArrayList<AppUser>();
            Pageable paging = PageRequest.of(page, size, Sort.by(sortby).descending());
            Page<AppUser> pageTuts;
            pageTuts = userServiceImpl.findAll(paging);
            if (email == null)
                pageTuts = userServiceImpl.findAll(paging);
            else
                pageTuts = userServiceImpl.findByEmailContaining(email, paging);
            tutorials = pageTuts.getContent();
            Map<String, Object> response = new HashMap<>();
            response.put("tutorials", tutorials);
            response.put("currentPage", pageTuts.getNumber());
            response.put("totalItems", pageTuts.getTotalElements());
            response.put("totalPages", pageTuts.getTotalPages());
            return new ResponseModel(true, response);
        } catch (Exception e) {
            return new ResponseModel(false, e.toString());
        }
    }

    @GetMapping("/require_role_USER")
    public ResponseEntity<?> require_role_USER() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = mapper.readTree("{\"Greeting\": \"You are USER\"}");
        return ResponseEntity.ok(json);
    }

    @GetMapping("/require_role_ADMIN")
    public ResponseEntity<?> require_role_ADMIN() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode json = mapper.readTree("{\"Greeting\": \"You are ADMIN\"}");
        return ResponseEntity.ok(json);
    }

    @GetMapping("/{id}")
    public ResponseModel one(@PathVariable Long id) {
        try {
            AppUser usr = userServiceImpl.findById(id);
            return new ResponseModel(true, usr);
        } catch (Exception e) {
            return new ResponseModel(false, null, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseModel updateOne(@PathVariable Long id, @RequestBody AppUser newUser) {
        try {
            AppUser usr = userServiceImpl.updateUser(id, newUser);
            return new ResponseModel(true, usr);
        } catch (Exception e) {
            return new ResponseModel(false, null, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseModel deleteUser(@PathVariable Long id) {
        try {
            userServiceImpl.deleteAppUser(id);
            return new ResponseModel(true, null);
        } catch (Exception e) {
            return new ResponseModel(false, null, e.getMessage());
        }
    }
}