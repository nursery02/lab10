package vn.tinylab.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.tinylab.entity.Product;
import vn.tinylab.model.request.ProductRequest;
import vn.tinylab.service.ProductService;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
@Api(tags = "ProductController")
public class ProductController {
    private final ProductService productService;

    @ApiOperation(value = "Create a product")
    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody ProductRequest request) {
        Product createdProduct = productService.createProduct(request);
        return new ResponseEntity<>(createdProduct, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Update a product")
    @PutMapping("{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable long id, @RequestParam long quantity) {
        Product updatedProduct = productService.updateProduct(id, quantity);
        return new ResponseEntity<>(updatedProduct, HttpStatus.OK);
    }
}
