package vn.tinylab.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.tinylab.entity.CompositeKey;
import vn.tinylab.entity.Order;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, CompositeKey> {
    List<Order> findByCompositeKey_ProductId(long productId);

    List<Order> findByCompositeKey_CustomerId(long customerId);
}
