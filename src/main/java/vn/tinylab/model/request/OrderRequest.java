package vn.tinylab.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderRequest {
    private Long customerId;
    private Long productId;
    private Long quantity;
}
